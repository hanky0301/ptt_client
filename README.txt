Testing platform: ptt

Implementation: login, logout, send message, send mail, post. (implementation details are written 
				as comments in ptt_client.c)

Type "make" to compile and "make run" to execute. (tested on linux system)

"input.txt" should be placed in the working directory.

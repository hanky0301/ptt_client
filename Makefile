all:
	gcc ptt_client.c -o ptt_client.o

run:
	@./ptt_client.o

clean:
	rm -f *~ ptt_client.o

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#define BUF_SIZE 4096
#define TMP_SIZE 32768
#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

char ENTER[10] = "\r";
char CTRL_C[10] = "\003";
char CTRL_U[10] = "\025";
char CTRL_X[10] = "\030";
char CTRL_P[10] = "\020";
char LEFT[10] = "\033[D";
char END[10] = "\033[F";

/* connect with ptt.cc and return the fd for connection */
int make_connection();
void fill_in_single_line(char buf[], char type[], int connect_fd);
void press(char buf[], int connect_fd);
void press_ten_times(char buf[], int connect_fd);
void login(FILE* fp, char buf[], int connect_fd);
void logout(int connect_fd);
void visit_board(char buf[], int connect_fd);
void post(FILE* fp, char buf[], int connect_fd);
void send_message(FILE* fp, char buf[], int connect_fd);
void send_mail(FILE* fp, char buf[], int connect_fd);
void post(FILE* fp, char buf[], int connect_fd);
void push(FILE* fp, char buf[], int connect_fd);

int main(int argc, char* argv[])
{
	int connect_fd = make_connection();
	char tmp[TMP_SIZE];
	if (read(connect_fd, tmp, TMP_SIZE) < 0)
		handle_error("read error");

	FILE* fp;
	if ((fp = fopen("input.txt", "r")) == NULL)
		handle_error("fopen error");

	char buf[BUF_SIZE];
	while (fgets(buf, BUF_SIZE, fp) != NULL)
	{
		if (strstr(buf, "<ID>") != NULL)
			login(fp, buf, connect_fd);
		else if (strstr(buf, "<EXIT>") != NULL)
			logout(connect_fd);
		else if (strstr(buf, "<BOARD>") != NULL)
			visit_board(buf, connect_fd);
		else if (strstr(buf, "<P>") != NULL)
			post(fp, buf, connect_fd);
		else if (strstr(buf, "<X>") != NULL)
			push(fp, buf, connect_fd);
		else if (strstr(buf, "<W>") != NULL)
			send_message(fp, buf, connect_fd);
		else if (strstr(buf, "<M>") != NULL)
			send_mail(fp, buf, connect_fd);
		else if (strstr(buf, "<EXIT>") != NULL)
			logout(connect_fd);
		else
			continue;
	}

	return 0;
}

int make_connection()
{
	int fd;
	struct sockaddr_in svr;

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		handle_error("socket error");
	
	svr.sin_family = AF_INET;
	svr.sin_addr.s_addr = inet_addr("140.112.172.3");
	svr.sin_port = htons(23);

	if (connect(fd, (struct sockaddr*) &svr, sizeof(svr)) < 0)
		handle_error("connect error");
	
	return fd;
}

void press(char buf[], int connect_fd)
{
	char tmp[TMP_SIZE];
	if (write(connect_fd, buf, strlen(buf)) < 0)
		handle_error("write error");
	if (read(connect_fd, tmp, TMP_SIZE) < 0)
		handle_error("read error");
	
	return;
}

void press_ten_times(char buf[], int connect_fd)
{
	int i;
	char tmp[TMP_SIZE];

	for (i = 0; i < 10; i++)
		if (write(connect_fd, buf, strlen(buf)) < 0)
			handle_error("write error");

	return;
}

void fill_in_single_line(char buf[], char type[], int connect_fd)
{
	/* <[type]>[data]</[type]> */
	char end_tag[30];
	sprintf(end_tag, "</%s>", type);
	char* data = buf + strlen(type) + 2;		
	int length = strstr(buf, end_tag) - data;	

	int i;
	char tmp[TMP_SIZE];
	for (i = 0; i < length; i++)
	{
		if (write(connect_fd, data + i, 1) < 0)
			handle_error("write error");
		if (strcmp(type, "PASS") != 0)
			if (read(connect_fd, tmp, TMP_SIZE) < 0)
				handle_error("read error");
	}

	return;
}

void fill_in_multiple_lines_content(FILE* fp, int connect_fd)
{
	int i;
	char tmp[TMP_SIZE];
	char buf[BUF_SIZE];
	fgets(buf, BUF_SIZE, fp);	// read the first line of the content from input.txt
	char* line = buf + strlen("<CONTENT>");
	int length;

	/* if the line does not contain "</CONTENT>", send the line and 
	 * read the next line from input.txt */
	while (strstr(buf, "</CONTENT>") == NULL)
	{
		length = strlen(line) - 1;
		for (i = 0; i < length; i++)
		{
			if (write(connect_fd, line + i, 1) < 0)
				handle_error("write error");
			if (read(connect_fd, tmp, TMP_SIZE) < 0)
				handle_error("read error");
		}
		press(ENTER, connect_fd);
		fgets(buf, BUF_SIZE, fp);
		line = buf;
	}
	/* if the line does contain "</CONTENT>", send the line and the function returns. */
	length = strstr(buf, "</CONTENT>") - line;
	for (i = 0; i < length; i++)
	{
		fprintf(stderr, "%c", line[i]);
		if (write(connect_fd, line + i, 1) < 0)
			handle_error("write error");
		if (read(connect_fd, tmp, TMP_SIZE) < 0)
			handle_error("read error");
	}

	return;
}

void login(FILE* fp, char buf[], int connect_fd)
{
	fprintf(stderr, "Logging in.\n");
	fill_in_single_line(buf, "ID", connect_fd);		// fill in the id
	press(ENTER, connect_fd);						// press enter
	
	fgets(buf, BUF_SIZE, fp);						// read password from input.txt
	fill_in_single_line(buf, "PASS", connect_fd);	// fill in the password
	press(ENTER, connect_fd);						// press enter

	press_ten_times(CTRL_C, connect_fd);			// press ctrl-c ten times
	return;
}

void logout(int connect_fd)
{
	fprintf(stderr, "Logging out.\n");
	press_ten_times(LEFT, connect_fd);				// press left arrow ten times

	press("g", connect_fd);							// press 'g'
	press(ENTER, connect_fd);						// press enter
	press("y", connect_fd);							// press 'y'
	press(ENTER, connect_fd);						// press enter

	press(CTRL_C, connect_fd);						// press ctrl-c ten times
	return;
}

void visit_board(char buf[], int connect_fd)
{
	fprintf(stderr, "Visiting board.\n");
	press_ten_times(LEFT, connect_fd);				// press left arrow ten times
	
	press("s", connect_fd);							// press 's'
	fill_in_single_line(buf, "BOARD", connect_fd);	// fill in the board name
	press(ENTER, connect_fd);						// press enter

	press_ten_times(CTRL_C, connect_fd);			// press ctrl-c ten times
	return;
}

void send_message(FILE* fp, char buf[], int connect_fd)
{
	fprintf(stderr, "Sending message.\n");
	press(CTRL_U, connect_fd);						// press ctrl-u

	press("s", connect_fd);							// press 's'
	fill_in_single_line(buf, "W", connect_fd);		// fill in the receiver id
	press(ENTER, connect_fd);						// press enter

	press("w", connect_fd);							// press 'w'
	fgets(buf, BUF_SIZE, fp);						// read content from input.txt
	fill_in_single_line(buf, "CONTENT", connect_fd);// fill in the content
	press(ENTER, connect_fd);						// press enter

	press("y", connect_fd);							// press 'y'
	press(ENTER, connect_fd);						// press enter

	press(LEFT, connect_fd);						// press left arrow
	return;
}

void send_mail(FILE* fp, char buf[], int connect_fd)
{
	fprintf(stderr, "Sending mail.\n");
	press_ten_times(LEFT, connect_fd);				// press left arrow ten times

	press("m", connect_fd);							// press 'm'
	press(ENTER, connect_fd);						// press enter
	press("s", connect_fd);							// press 's'
	press(ENTER, connect_fd);						// press enter

	fill_in_single_line(buf, "M", connect_fd);		// fill in the receiver id
	press(ENTER, connect_fd);						// press enter
	
	fgets(buf, BUF_SIZE, fp);						// read title from input.txt
	fill_in_single_line(buf, "TITLE", connect_fd);	// fill in the title
	press(ENTER, connect_fd);						// press enter

	/* read content from input.txt and fill in the content */
	fill_in_multiple_lines_content(fp, connect_fd);	
	press(CTRL_X, connect_fd);						// press ctrl-x

	press("s", connect_fd);							// press s
	press(ENTER, connect_fd);						// press enter
	
	press_ten_times(CTRL_C, connect_fd);			// press ctrl-c ten times
	return;
}

void post(FILE* fp, char buf[], int connect_fd)
{
	fprintf(stderr, "Posting.\n");
	press(CTRL_P, connect_fd);						// press ctrl-p
	press(ENTER, connect_fd);						// press enter

	fill_in_single_line(buf, "P", connect_fd);		// fill in the title
	press(ENTER, connect_fd);						// press enter

	/* read content from input.txt and fill in the content */
	fill_in_multiple_lines_content(fp, connect_fd);
	press(CTRL_X, connect_fd);						// press ctrl-x

	press("s", connect_fd);							// press s
	press(ENTER, connect_fd);						// press enter
	
	press_ten_times(CTRL_C, connect_fd);			// press ctrl-c ten times
	return;
}

void push(FILE* fp, char buf[], int connect_fd)
{
	fprintf(stderr, "Pushing.\n");
	press("/", connect_fd);							// press '/'
	fill_in_single_line(buf, "X", connect_fd);		// fill in the keyword
	press(ENTER, connect_fd);						// press enter
	press(END, connect_fd);							// press End button
	press(LEFT, connect_fd);						// press left arrow

	press("X", connect_fd);								// press 'X'
	press(ENTER, connect_fd);							// press enter
	fgets(buf, BUF_SIZE, fp);							// read content from input.txt
	fill_in_single_line(buf, "CONTENT", connect_fd);	// fill in the content
	press(ENTER, connect_fd);							// press enter
	press("y", connect_fd);								// press enter
	press(ENTER, connect_fd);							// press enter
	
	return;
}
